// Read configurations
var config = require('./config');

// Load Sequelize package
var Sequelize = require("sequelize");

// Create Sequelize DB connection
var sequelize = new Sequelize(
	'shop',
	config.MYSQL_USERNAME,
	config.MYSQL_PASSWORD,
	{
		host: config.MYSQL_HOSTNAME,
		port: config.MYSQL_PORT,
		logging: config.MYSQL_LOGGING,
		dialect: 'mysql',
		pool: {
			max: 5,
			min: 0,
			idle: 10000,
		},
	}
);

// Import DB Models
const Grocery = sequelize.import('./models/grocery');


// Define Model Associations

// Exports Models
// In every js file, all the variables mentioned within the variables are local variables. Someone that requires the JS files will not have access into anything here
// We do a export here to give access to the databse models (e.g department models) to somewhere else that might require 
// db.js is the one that links the entire app to the mysql database
module.exports = {
  // Loads model for Grocery table
  Grocery: Grocery,

};
