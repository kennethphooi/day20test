// Handles API routes

module.exports = function(app, db) {
  var Grocery = require('./api/grocery.controller')(db);

  // Retrieve all department records that match query string passed. 
  app.get("/api/grocerySearch", Grocery.retrieveProduct);
  app.get("/api/groceryEdit/:id", Grocery.getProduct);
  app.put("/api/groceryChange", Grocery.updateProduct);
};
