module.exports = function(sequelize, DataTypes) {
  return sequelize.define('grocery', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    upc12: {
      type: DataTypes.BIGINT(12),
      allowNull: false,
      primaryKey: false,
    },
    brand: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    }
  },
  {
    // don't add timestamps attributes updatedAt and createdAt
    timestamps: false,
    freezeTableName: true,
    tableName: "grocery_list",
  });

  return Grocery
};