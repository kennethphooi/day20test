// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

// Loads sequelize ORM
var Sequelize = require("sequelize");		//Can comment this out because it is being configured in db.js file (line 5)

// CONSTANTS --------------------------------------------------------------------------------------
const config = require('./config');

// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.

// const NODE_PORT = process.env.PORT || config.PORT || 3000;
// Commented out because we use line 19 for the AWS example
const NODE_PORT = process.env.NODE_PORT || config.PORT || 3000;		

// Defines paths
// __dirname is a global that holds the directory name of the current module
// CLIENT FOLDER is the public directory
const CLIENT_FOLDER = path.join(__dirname, '/../client');  
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

// Creates an instance of express called app
var app = express();

// Load Database Models
//This is a handle. So next time you can just type db.employees in other pages
const db = require('./db');

// MIDDLEWARES ------------------------------------------------------------------------------------

// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// Populates req.body with information submitted through the registration form.
// Default $http content type is application/json so we use json as the parser type
// For content type application/x-www-form-urlencoded,
// use: app.use(bodyParser.urlencoded({extended: false}));
//Used to change your POST into a JSOn format and put it in the request body
app.use(bodyParser.json());


// ROUTE HANDLERS ---------------------------------------------------------------------------------
// Load Routes handlers
//route.js now handles all the routes. But you need to parse in the Express app (app) and the database (db)
const routes = require('./routes')(app, db);


// ERROR HANDLING ---------------------------------------------------------------------------------
// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function(req, res) {
	res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function(err, req, res, next) {
	res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
});

// SERVER / PORT SETUP ----------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function() {
	console.log("Server running at http://localhost:" + NODE_PORT);
});
