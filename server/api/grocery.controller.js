// Retrieve from DB
var retrieveProduct = function(db) {
  return function(req, res) {
    db.Grocery
      .findAll({
        where: {
          $or: [
            { brand: { $like: "%" + req.query.searchString + "%" } },
            { name: { $like: "%" + req.query.searchString + "%" } }
          ]
        },
        limit: 20,
        order:[["brand", "DESC"],
        // ["brand", "ASC"],
        // ["name", "DESC"],
        // ["name", "ASC"],
        ] 
      })
      .then(function (grocery) {
        res
          .status(200)
          .json(grocery);
      })
      .catch(function (err) {
        res
          .status(500)
          .json(err);
      });
  }
};

var getProduct = function(db){
    return function(req, res) {

    db.Grocery
        .findOne({
            where: {id:req.params.id}
        })
      
        .then(function (grocery) {
            console.log("-- GET /api/groceryEdit/:id findOne then() result \n " + JSON.stringify(grocery));
            res.json(grocery);
        })
   
        .catch(function (err) {
            console.log("-- GET /api/groceryEdit/:id findOne catch() \n " + JSON.stringify(grocery));
            res
                .status(500)
                .json({error: true});
        });
    }
}

var updateProduct = function (db) {
    return function (req, res) {
    db.Grocery
    var where = {};
    where.id = req.params.id;

    // *** Updates department detail
    
    };
}

module.exports = function(db) {
  return {
    retrieveProduct: retrieveProduct(db),
    getProduct: getProduct(db),
    updateProduct: updateProduct(db),
  }
};