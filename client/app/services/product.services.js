// Always use an IIFE, i.e., (function() {})();
(function() {
  // Attaches DeptService service to the DMS module
  angular
    .module("GRO")
    .service("ProdService", ProdService);

  // Dependency injection. Here we inject $http because we need this built-in service to 
  // communicate with the server. There are different ways to inject dependencies;
  // $inject is minification safe
  ProdService.$inject = ['$http'];

  // DeptService function declaration
  // Accepts the injected dependency as a parameter. We name it $http for consistency, 
  // but you may assign any name
  function ProdService($http) {
    // Declares the var service and assigns it the object this (in this case, the DeptService). 
    // Any function or variable that you attach to service will be exposed to callers of 
    // DeptService, e.g., search.controller.js and register.controller.js
    var service = this;

    // Exposed data models ------------------------------------------------------------------------
    // Exposed functions --------------------------------------------------------------------------
    service.retrieveProduct = retrieveProduct;
    service.retrieveProdByID= retrieveProdByID;

    // Function declaration and definition --------------------------------------------------------

    // insertDept uses HTTP POST to send department information to the server's /departments route
    // Parameters: department information; Returns: Promise object
   
    function retrieveProduct(searchString) {
      return $http.get(
        '/api/grocerySearch', {
        params: {
          'searchString': searchString,
        }
      });
    }

    function retrieveProdByID(id) {
            return $http({
                method: 'GET'
                , url: "api/groceryEdit/" + id
            });
        }



  }
})();
