// Always use an IIFE, i.e., (function() {})();
(function() {
  // Attaches a SearchCtrl to the DMS module
  angular
    .module("GRO")
    .controller("SearchCtrl", SearchCtrl);

  // Dependency injection. An empty [] means RegCtrl does not have dependencies. 
  // Here we inject DeptService so SeatchCtrl can call services related to department.
  SearchCtrl.$inject = ['ProdService', '$state'];

  // Search function declaration
  function SearchCtrl (ProdService, $state) {
    // Declares the var vm (for ViewModel) and assigns it the object this (in this case, 
    // the SearchCtrl). Any function or variable that you attach to vm will be exposed 
    // to callers of SearchCtrl, e.g., search.html
    var vm = this;

    // Exposed data models ------------------------------------------------------------------------
    vm.searchString = '';
    vm.result = null;
    vm.showProd = false;
    vm.searchForProduct= searchForProduct;

    // Exposed functions --------------------------------------------------------------------------
    // Exposed functions can be called from the view. Currently, search.controller.js doesn't have
    // any exposed functions.

    // Initializations ----------------------------------------------------------------------------
    // Functions that are run when view/html is loaded
    // init is a private function (i.e., not exposed)
    init();

    // Function declaration and definition --------------------------------------------------------
    // The init function initializes view
    function init() {
      // We call DeptService.retrieveDept to handle retrieval of department information. The data retrieved from
      // this function is used to populate search.html.
      ProdService
        .retrieveProduct('')
        .then(function (results) {
         
          vm.grocery = results.data;
        })
        .catch(function (err) {
         
          console.log("error " + err);
        });
    }

    function searchForProduct() {
      vm.showProd = true;
      ProdService
        // we pass contents of vm.searchString to service so that we can search the DB for this 
        // string
        .retrieveProduct(vm.searchString)
        .then(function (results) {
          // The result returned by the DB contains a data object, which in turn contains the 
          // records read from the database
          console.log("results: " + JSON.stringify(results.data));
          vm.grocery = results.data;
        })
        .catch(function (err) {
          // We console.log the error. For a more graceful way of handling the error, see
          // register.controller.js
          console.info("error " + JSON.stringify(err));
        });
    }
  }
})();
