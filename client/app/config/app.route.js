(function(){
     angular
        .module('GRO')
        .config(uiRouteConfig);

    uiRouteConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function uiRouteConfig($stateProvider, $urlRouterProvider){
        $stateProvider
        .state('search', {
            url: '/search',
            templateUrl: 'app/search/search.html'
        })
        .state('edit', {
            url: '/edit',
            templateUrl: 'app/edit/edit.html'
        })
        

        $urlRouterProvider.otherwise('/search');          //This is a catch-all function. If the request can't be found, just show the /register page
    }

})();